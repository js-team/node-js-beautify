# Installation
> `npm install --save @types/js-beautify`

# Summary
This package contains type definitions for js-beautify (https://github.com/beautify-web/js-beautify/).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/js-beautify.

### Additional Details
 * Last updated: Tue, 07 Nov 2023 03:09:37 GMT
 * Dependencies: none

# Credits
These definitions were written by [Hans Windhoff](https://github.com/hansrwindhoff), [Gavin Rehkemper](https://github.com/gavinr), and [Piotr Błażejewicz](https://github.com/peterblazejewicz).
